# Описание проекта 

WebAPPAuth - приложение для авторизации пользователей в веб приложении. Для защиты используется jwt.
Для решения проблемы релогина, используются два токена ```refresh_token``` и ```access_token```. При 
смене пароля генерируется ```password_token``` который является частью url для смены пароля.

#### Приложение содержит следующие роуты:
* register - для регистрации новых пользователей 
* refresh - для обновления токена в случае если access_token не валиден
* login - для авторизации пользователя
* reset-password-mail - для отправки сообщения на почту о сбросе пароля
* reset-password/{password_token} - для проверки валидности ссылки для смены пароля
* reset_password - для замены пароля

#### Приложение содержит следующие методы для проверки валидности и генерации токенов:
* generate_tokens - функция для генерации токенов доступа и обновления
* generate_password_token - функция для генерации токена смены пароля
* is_password_token_valid - функция для проверки валидности токена смены пароля
* is_access_token_valid - функция для проверки валидности токена доступа
* is_refresh_token_valid - функция для проверки валидности токена обновления
* refresh_access_token - функция для обновления токена доступа
* take_token_data - функция для извлечения данных из токена для смены пароля
* is_valid_email - функция для проверки валидности почты
* is_valid_password - функция для проверки валидности почты

# Установка библиотек и зависимостей

* Устанавливаем необходимые библиотеки с помощью команды ```pip install -r requirements.txt```
* Создаем файл ```.env```  и задаем в нем следующие переменные
```
SECRET_KEY=<SECRET_KEY>
ACCESS_TOKEN_EXPIRE_MINUTES=<ACCESS_TOKEN_EXPIRE_MINUTES>
REFRESH_TOKEN_EXPIRE_DAYS=<REFRESH_TOKEN_EXPIRE_DAYS>

PASSWORD_TOKEN_EXPIRE_MINUTES=<PASSWORD_TOKEN_EXPIRE_MINUTES>

DB_HOST=<DB_HOST>
DB_PORT=<DB_PORT>
DB_NAME=<DB_NAME>
DB_USER=<DB_USER>
DB_PASS=<DB_PASS>

SMTP_USERNAME=<SMTP_USERNAME>
SMTP_PASSWORD=<SMTP_PASSWORD>
SMTP_EMAIL=<SMTP_EMAIL>
SMTP_PORT=<SMTP_PORT>

ADDRESS_API=<ADDRESS_API> #не забыть в конце про /
```

# Создание бд и миграций 

* Создать бд в postgres
* Прописать команду ```alembic init migration``` для создания конфигурационных файлов для выполнения миграции
* Редактируем файлы ```alembic.ini, migrations/env.py```
* Прописываем ```alembic revision --autogenerate -m "Database creation"``` для создания скрипта миграций
* Прописываем ```alembic upgrade {необходимые ключ из миграции}``` для обновления бд до определенной миграции

# Запуск веб приложения

* Для запуска приложения прописываем команду ```uvicorn app:app --reload```
* Для доступа ко всем методам переходим по адресу <http://127.0.0.1:8000/docs>

