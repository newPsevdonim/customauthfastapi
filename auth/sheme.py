from fastapi import Form
from pydantic import BaseModel

from typing import Optional

import datetime


class UserCreate(BaseModel):
    login: str
    password: str
    mail: str


class UserLogin(BaseModel):
    login_email_phone: str
    password: str
