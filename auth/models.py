from sqlalchemy import create_engine, Column, Integer, String, DateTime, BOOLEAN
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from datetime import datetime, timedelta

# Модель пользователя
from database import Base


class User(Base):
    __tablename__ = "User"

    id = Column(Integer, primary_key=True, autoincrement=True)
    login = Column(String)
    password = Column(String)
    mail = Column(String)
    platform = Column(String, default="WEB APP")
    rights = Column(String, default="user")
    register_at = Column(DateTime, default=datetime.utcnow())
    rights_end = Column(DateTime, default=datetime.utcnow())
    new_user_number = Column(Integer, default=0)
    is_admin = Column(BOOLEAN, default=False)
    disabled = Column(BOOLEAN, default=False)

    def __repr__(self):
        return (
            f"<User(id={self.id},"
            f" login='{self.login}',"
            f" email='{self.mail}',"
            f" status='{self.rights}')>"
        )
