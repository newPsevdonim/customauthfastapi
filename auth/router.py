from fastapi import (
    FastAPI,
    HTTPException,
    Depends,
    Response,
    status,
    Cookie,
    Form,
    APIRouter,
)
from passlib.context import CryptContext
from sqlalchemy import insert, select, update
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.exc import SQLAlchemyError
from starlette.responses import JSONResponse

from database import get_async_session, redis_url
from auth.manager import (
    generate_tokens,
    is_access_token_valid,
    is_refresh_token_valid,
    refresh_access_token,
    generate_password_token,
    is_password_refresh_token_valid,
    take_token_data,
    is_valid_email,
    is_valid_password,
)
from config import (
    ACCESS_TOKEN_EXPIRE_MINUTES,
    REFRESH_TOKEN_EXPIRE_DAYS,
    PASSWORD_TOKEN_EXPIRE_MINUTES,
    ADDRESS_API,
)
from auth.sheme import UserCreate, UserLogin
from auth.models import User
from mail.manager import send_email

password_valid_ex = (
    "invalid password. the password must be at least 8."
    "the password must contain uppercase and lowercase characters."
    " the password must contain !@#?_=+$%^&*(),./|"
)

router = APIRouter(prefix="/auth", tags=["auth"])

password_context = CryptContext(schemes=["bcrypt"], deprecated="auto")


# маршрут для регистрации пользователя
@router.post("/register")
async def register(
        response: Response,
        user: UserCreate,
        session: AsyncSession = Depends(get_async_session),
):
    if not is_valid_email(user.mail):
        return JSONResponse(
            status_code=400,
            content={
                "status": "error",
                "data": f"Failed to execute: invalid username/email/phone",
                "details": f"invalid username/email/phone",
                "error_details": f"invalid username/email/phone",
            },
        )

    if not is_valid_password(user.password):
        return JSONResponse(
            status_code=400,
            content={
                "status": "error",
                "data": f"Failed to execute: {password_valid_ex}",
                "details": f"{password_valid_ex}",
                "error_details": f"{password_valid_ex}",
            },
        )

    stmt = select(User.id).where(User.login == user.login)
    result = await session.execute(stmt)
    if result.scalar() is not None:
        return JSONResponse(
            status_code=400,
            content={
                "status": "error",
                "data": f"Failed to execute: login already registered",
                "details": "login already registered",
                "error_details": "login already registered",
            },
        )

    stmt = select(User.id).where(User.login == user.mail)
    result = await session.execute(stmt)
    if result.scalar() is not None:
        return JSONResponse(
            status_code=400,
            content={
                "status": "error",
                "data": f"Failed to execute: email already used",
                "details": "email already used",
                "error_details": "email already used",
            },
        )

    user.password = password_context.hash(user.password)
    user_data = user.dict()
    stmt = insert(User).values(**user_data)
    await session.execute(stmt)
    await session.commit()

    email_text = f"""
    Спасибо, что зарегистрировались. Мы ценим новых пользователей.
    """
    send_email(user.mail, email_text)

    return {"status": "success"}


# маршрут для обновления токена доступа
@router.post("/refresh")
async def refresh(
        response: Response,
        access_token: str = Cookie("access_token"),
        refresh_token: str = Cookie("refresh_token"),
):
    if is_access_token_valid(access_token) is True:
        return {
            "status": 200,
            "access_token": access_token,
            "detail": "The token has not expired",
        }
    if not refresh_token:
        return JSONResponse(
            status_code=401,
            content={
                "status": "error",
                "data": f"Invalid refresh token",
                "details": "Invalid refresh token",
                "error_details": "Invalid refresh token",
            },
        )
    if is_refresh_token_valid(refresh_token) is False:
        return JSONResponse(
            status_code=401,
            content={
                "status": "error",
                "data": f"Invalid refresh token",
                "details": "Invalid refresh token",
                "error_details": "Invalid refresh token",
            },
        )
    access_token = refresh_access_token(refresh_token)
    response.set_cookie(
        key="access_token",
        value=access_token,
        httponly=True,
        max_age=ACCESS_TOKEN_EXPIRE_MINUTES * 60,
    )
    response = {
        "access_token": access_token,
        "status": 200,
        "detail": "The token create",
    }
    return response


# маршрут для авторизации пользователя
@router.post("/login")
async def login(
        response: Response,
        value: UserLogin,
        session: AsyncSession = Depends(get_async_session),
):
    stmt = select(User).where(User.login == value.login_email)
    user = await session.execute(stmt)
    user = user.scalar()
    if not user:
        stmt = select(User).where(User.mail == value.login_email)
        user = await session.execute(stmt)
        user = user.scalar()

    if not user:
        return JSONResponse(
            status_code=404,
            content={
                "status": "error",
                "data": f"User not found",
                "details": "User not found",
                "error_details": "User not found",
            },
        )

    if user.disabled:
        return JSONResponse(
            status_code=403,
            content={
                "status": "error",
                "data": f"User ban",
                "details": "User ban",
                "error_details": "User ban",
            },
        )

    if password_context.verify(value.password, user.password) is False:
        return JSONResponse(
            status_code=401,
            content={
                "status": "error",
                "data": f"Incorrect password",
                "details": "Incorrect password",
                "error_details": "Incorrect password",
            },
        )

    access_token, refresh_token = generate_tokens(user.login, user.id, user.mail)
    response.set_cookie(
        key="access_token",
        value=access_token,
        httponly=True,
        max_age=ACCESS_TOKEN_EXPIRE_MINUTES * 60,
    )
    response.set_cookie(
        key="refresh_token",
        value=refresh_token,
        httponly=True,
        max_age=REFRESH_TOKEN_EXPIRE_DAYS * 24 * 60 * 60,
    )
    response = {"access_token": access_token, "refresh_token": refresh_token}
    return response


@router.get("/logout")
async def logout(
        response: Response,
        value: UserLogin,
        session: AsyncSession = Depends(get_async_session),
):
    response.set_cookie(
        key="access_token",
        value="delete",
        httponly=True,
        max_age=0,
    )
    response.set_cookie(
        key="refresh_token",
        value="delete",
        httponly=True,
        max_age=0,
    )

    return {"status": "success"}


# маршрут для генерации токена и отправки email при смене пароля
@router.get("/reset-password-mail/")
async def reset_password_mail(
        mail: str, session: AsyncSession = Depends(get_async_session)
):
    stmt = select(User).where(User.mail == mail)
    result = await session.execute(stmt)
    result = result.scalar()
    if result is None or result == "":
        print("ok")
        return {"status_code": 404, "detail": "Email not found"}

    password_token = generate_password_token(mail)

    redis_url.hset('reset_password_token', mail, password_token)

    password_url = (
        f"{ADDRESS_API}auth/reset-password/is_valid_password_token/{password_token}"
    )
    email_text = f"""
    Для смены пароля перейдите по ссылке:
    {password_url}
    Если вы передумали менять пароль, то проигнорируйте данное сообщение
    """
    send_email(mail, email_text)
    return {"status": "success", "password_token": password_token}


# маршрут для проверки валидности токена по смене пароля
@router.get("/reset-password/is_valid_password_token/{password_token}")
async def valid_reset_password(response: Response, password_token: str):
    if is_password_refresh_token_valid(password_token) is True:
        response.set_cookie(
            key="password_token",
            value=password_token,
            httponly=True,
            max_age=PASSWORD_TOKEN_EXPIRE_MINUTES * 60,
        )
        all_tokens = {key.decode('utf-8'): value.decode('utf-8') for key, value in
                      redis_url.hgetall('reset_password_token').items()}
        print(all_tokens)
        if password_token in list(all_tokens.values()):
            keys = list(all_tokens.keys())
            index = list(all_tokens.values()).index(f"{password_token}")
            key = keys[index]
            redis_url.hdel('reset_password_token', key)
            return {"status": "token valid"}
        else:
            raise HTTPException(status_code=401, detail="Invalid refresh token")
    else:
        raise HTTPException(status_code=401, detail="Invalid refresh token")


# маршрут для смены пароля
@router.put("/reset-password")
async def reset_password(
        response: Response,
        password: str,
        password_copy: str,
        password_token: str = Cookie("password_token"),
        session: AsyncSession = Depends(get_async_session),
):
    email = take_token_data(password_token)
    if is_valid_password(password) is False:
        raise HTTPException(status_code=400, detail=password_valid_ex)
    if password_copy == password:
        password = password_context.hash(password)
        stmt = update(User).where(User.mail == email).values({"password": password})
        await session.execute(stmt)
        await session.commit()
    return {"status": "success"}
