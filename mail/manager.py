import smtplib
from email.mime.text import MIMEText

from config import SMTP_USERNAME, SMTP_PASSWORD, SMTP_EMAIL, SMTP_PORT


def send_email(email, text):
    # Создаем объект MIMEText для форматирования email-сообщения
    message = MIMEText(text)

    # Задаем параметры email-сообщения
    message["Subject"] = "Password recovery"
    message["From"] = SMTP_USERNAME
    message["To"] = email

    # Соединяемся с SMTP-сервером, производим авторизацию, отправляем email-сообщение и
    # отключаемся
    with smtplib.SMTP_SSL(SMTP_EMAIL, SMTP_PORT) as server:
        server.login(SMTP_USERNAME, SMTP_PASSWORD)
        server.sendmail(SMTP_USERNAME, email, message.as_string())
