from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from auth.router import router as router_auth

app = FastAPI(title="Auth App")

app.include_router(router_auth)

origins = [
    "http://localhost:3000",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
