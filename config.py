from dotenv import load_dotenv
import os

load_dotenv()

DB_HOST = os.environ.get("DB_HOST")
DB_PORT = os.environ.get("DB_PORT")
DB_NAME = os.environ.get("DB_NAME")
DB_USER = os.environ.get("DB_USER")
DB_PASS = os.environ.get("DB_PASS")

REDIS_HOST = os.environ.get("REDIS_HOST")
REDIS_PORT = os.environ.get("REDIS_PORT")

SECRET_KEY = os.environ.get("SECRET_KEY")
ACCESS_TOKEN_EXPIRE_MINUTES = os.environ.get("ACCESS_TOKEN_EXPIRE_MINUTES")
REFRESH_TOKEN_EXPIRE_DAYS = os.environ.get("REFRESH_TOKEN_EXPIRE_DAYS")
PASSWORD_TOKEN_EXPIRE_MINUTES = os.environ.get("PASSWORD_TOKEN_EXPIRE_MINUTES")

SMTP_USERNAME = os.environ.get("SMTP_USERNAME")
SMTP_PASSWORD = os.environ.get("SMTP_PASSWORD")
SMTP_EMAIL = os.environ.get("SMTP_EMAIL")
SMTP_PORT = os.environ.get("SMTP_PORT")

ADDRESS_API = os.environ.get("ADDRESS_API")

if DB_HOST is None:
    raise Exception("The variable is not set: DB_HOST")
if DB_PORT is None:
    raise Exception("The variable is not set: DB_PORT")
if DB_NAME is None:
    raise Exception("The variable is not set: DB_NAME")
if DB_USER is None:
    raise Exception("The variable is not set: DB_USER")
if DB_PASS is None:
    raise Exception("The variable is not set: DB_PASS")

if SECRET_KEY is None:
    raise Exception("The variable is not set: SECRET_KEY")
if ACCESS_TOKEN_EXPIRE_MINUTES is None:
    raise Exception("The variable is not set: ACCESS_TOKEN_EXPIRE_MINUTES")
if REFRESH_TOKEN_EXPIRE_DAYS is None:
    raise Exception("The variable is not set: REFRESH_TOKEN_EXPIRE_DAYS")
if PASSWORD_TOKEN_EXPIRE_MINUTES is None:
    raise Exception("The variable is not set: PASSWORD_TOKEN_EXPIRE_MINUTES")

if SMTP_USERNAME is None:
    raise Exception("The variable is not set: SMTP_USERNAME")
if SMTP_PASSWORD is None:
    raise Exception("The variable is not set: MTP_PASSWORD")
if SMTP_EMAIL is None:
    raise Exception("The variable is not set: SMTP_EMAIL")
if SMTP_PORT is None:
    raise Exception("The variable is not set: SMTP_PORT")

if ADDRESS_API is None:
    raise Exception("The variable is not set: ADDRESS_API")
